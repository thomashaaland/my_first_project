# Sandworm from Arrakis

The picture called arrakis.jpg is a picture of a sandworm from the planet Arrakis. They are the producers of the spice Melange which allows Navigators to find their way in space.

![Sandworm](arrakis.jpg)
